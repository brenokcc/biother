# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from djangoplus.decorators.views import view


@view('Home', login_required=False)
def home(request):
    return locals()


@view('Fate', login_required=False)
def particles(request):
    return locals()


@view('Biocamouflage', login_required=False)
def nano(request):
    return locals()


@view('Combined Nano-Therapies', login_required=False)
def combined_nano(request):
    return locals()


@view('Magnetic Tissue Engineering', login_required=False)
def magnetic(request):
    return locals()


@view('Intracellular Nano-Fate', login_required=False)
def nano_fate(request):
    return locals()


@view('Oncology', login_required=False)
def oncology(request):
    return locals()


@view('Contact', login_required=False)
def contact(request):
    return locals()


@view('Funding', login_required=False)
def funding(request):
    return locals()


@view('News', login_required=False)
def news(request):
    return locals()


@view('Gallery', login_required=False)
def gallery(request):
    return locals()


@view('Jobs', login_required=False)
def jobs(request):
    return locals()


@view('Partners', login_required=False)
def partners(request):
    return locals()


@view('Publications', login_required=False)
def publications(request):
    return locals()


@view('Team', login_required=False)
def team(request):
    return locals()



