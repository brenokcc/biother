# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DROPBOX_TOKEN = '' # disponível em https://www.dropbox.com/developers/apps
DROPBOX_LOCALDIR = MEDIA_ROOT
DROPBOX_REMOTEDIR = '/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if not exists(join(BASE_DIR, 'logs')):
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

DIGITAL_OCEAN_SERVER = '104.236.79.20'
DIGITAL_OCEAN_DOMAIN = 'biother.net'
DIGITAL_OCEAN_TOKEN = 'f0e9844cdb9e0dc3705c13ff84c83d65593538b04a3869b1df4b567603f2bc9e'



