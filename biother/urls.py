# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url, include
from djangoplus.admin.views import index
from biother.views import home

urlpatterns = [
    url(r'^$', home),
    url(r'^admin/$', index),
    url(r'', include('djangoplus.admin.urls')),
]


